const express = require('express');
const db = require('../fileDb');

const router = express.Router();

router.get('/', (req, res) => {
        res.send(db.getItems(req.query.datetime))
});

router.post('/', (req,res) => {
    if(req.body.message &&  req.body.author) {
        db.addItem(req.body);
        res.status(201).send({message: 'OK'});
    } else {
        res.status(400).send({message: 'Enter author name and message'});
    }

});

module.exports = router;