const fs = require('fs');
const nanoid = require('nanoid');

const filename = './db.json';

let data = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(filename);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },
    getItems(date) {
        if (date) {
            return data.filter(item => item.date > date )
        }
        return data;
    },
    addItem(item) {
        item.date = new Date().toISOString();
        item.id = nanoid();
        data.push(item);
        this.save();
    },
    save() {
        fs.writeFileSync(filename, JSON.stringify(data, null,2));
    }
};